/*global jQuery:false */
jQuery(document).ready(function($) {
  "use strict";

  //==== Customize =====
  $('.gavias-skins-panel .control-panel').click(function(){
      if($(this).parents('.gavias-skins-panel').hasClass('active')){
          $(this).parents('.gavias-skins-panel').removeClass('active');
      }else $(this).parents('.gavias-skins-panel').addClass('active');
  });

  // Add class for reply comment.
  if($('section div').hasClass('indented')) {
    $(this).find('div.indented div.js-comment').addClass('comment-reply');
  }

});
