<?php

/**
 * @file
 * Various theme setting logic.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function flattern_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  // Get the build info for the form.
  $build_info = $form_state->getBuildInfo();
  // Get the theme name we are editing.
  $theme = \Drupal::theme()->getActiveTheme()->getName();

  $form['core'] = [
    '#type' => 'vertical_tabs',
    '#attributes' => ['class' => ['entity-meta']],
    '#weight' => -899,
  ];

  $form['theme_settings']['#group'] = 'core';
  $form['logo']['#group'] = 'core';
  $form['favicon']['#group'] = 'core';

  $form['theme_settings']['#open'] = FALSE;
  $form['logo']['#open'] = FALSE;
  $form['favicon']['#open'] = FALSE;

  // Custom settings in Vertical Tabs container.
  $form['options'] = [
    '#type' => 'vertical_tabs',
    '#attributes' => ['class' => ['entity-meta']],
    '#weight' => -999,
    '#default_tab' => 'edit-variables',
    '#states' => [
      'invisible' => [
        ':input[name="force_subtheme_creation"]' => ['checked' => TRUE],
      ],
    ],
  ];

  /*--------- Setting Top Header ------------ */
  $form['top_header'] = [
    '#type' => 'details',
    '#attributes' => [],
    '#title' => t('Top Header options'),
    '#weight' => -998,
    '#group' => 'options',
    '#open' => FALSE,
  ];

  $form['top_header']['header_short_email'] = [
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#default_value' => theme_get_setting('header_short_email'),
  ];

  $form['top_header']['header_office_contact'] = [
    '#type' => 'textfield',
    '#title' => t('Office contact number'),
    '#default_value' => theme_get_setting('header_office_contact'),
  ];

  $form['top_header']['header_office_location'] = [
    '#type' => 'textfield',
    '#title' => t('Office location'),
    '#default_value' => theme_get_setting('header_office_location'),
  ];

  /*--------- Setting Social share ------------ */
  $form['social_share'] = [
    '#type' => 'details',
    '#attributes' => [],
    '#title' => t('Social share settings'),
    '#weight' => -998,
    '#group' => 'options',
    '#open' => FALSE,
  ];

  $form['social_share']['twitter'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter link'),
    '#default_value' => theme_get_setting('twitter'),
  ];

  $form['social_share']['facebook'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook link'),
    '#default_value' => theme_get_setting('facebook'),
  ];

  $form['social_share']['instagram'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram link'),
    '#default_value' => theme_get_setting('instagram'),
  ];

  $form['social_share']['skype'] = [
    '#type' => 'textfield',
    '#title' => t('Skype Id'),
    '#default_value' => theme_get_setting('skype'),
  ];

  $form['social_share']['linkedin'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin link'),
    '#default_value' => theme_get_setting('linkedin'),
  ];

  $form['actions']['submit']['#value'] = t('Save');
}
