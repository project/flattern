Flattern – Multipurpose Bootstrap Business Profile
==================================================

Flattern is a clean, flat and professional Drupal theme created with latest 
version of bootstrap framework. Coming with flat, young, modern and trendy 
design this template will suitable for corporate business, agency, consulting 
business, startup company, finance business, insurance, loan, tax help, 
Investment firm etc.

Flattern Drupal theme is perfect solution and follow highly quality standards 
and technologies like Bootstrap framework, HTML5 & CSS3 also W3C valid html.
Flattern HTML template is fully responsive responsive and works perfectly on 
any screen.

Notes:
Installation of profile may take some time. If you get the “Maximum execution 
time...” error, need to increase the max_execution_time parameter in php.ini 
file.

To install the profile:

- Unzip the folder and copy it to the necessary folder of your server.
- Install your new website with the Flattern profile. It’s set by default, you 
 just need to click the “Save and continue” button and follow the instructions.
- Wait for the Flattern profile to be installed, enable the necessary modules
and enjoy your website.

Thank you for choosing the Flattern profile!

Created by Datamatics Global Services Limited

