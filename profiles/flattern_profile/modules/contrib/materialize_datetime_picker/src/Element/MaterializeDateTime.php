<?php

namespace Drupal\materialize_datetime_picker\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

/**
 * Provides a MaterializeDateTime form element.
 *
 * @FormElement("materialize_date_time")
 */
class MaterializeDateTime extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#maxlength' => 512,
      '#size' => 25,
      '#process' => [[$class, 'processMaterializeDateTime']],
      '#pre_render' => [[$class, 'preRenderMaterializeDateTime']],
      '#theme_wrappers' => ['form_element'],
      '#theme' => 'input__textfield',
    ];

  }

  /**
   * Render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderMaterializeDateTime(array $element) {
    Element::setAttributes($element, ['id', 'name', 'value', 'size']);
    static::setAttributes($element, ['form-date']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function processMaterializeDateTime(&$element, FormStateInterface $form_state, &$complete_form) {
    // Get system regional settings.
    $first_day = \Drupal::config('system.date')->get('first_day');
    if(!empty($element['#date_type'])) {
      $element = $element;
      $date_only = [];
      $datetime_only = [];
    }
    else {
      $datetime_config = \Drupal::config('materialize_datetime_picker.site_config_datetime')->get("materialize_datetime_data");

      $element['#date_type'] = $datetime_config['date_type_format'];
      $element['#exclude_date'] = $datetime_config['exclude_date'];
      $element['#week_start'] = $datetime_config['week_start'];
      $element['#disable_days'] = $datetime_config['disable_days'];
      $element['#allow_times'] = $datetime_config['allow_times'];
      $element['#hour_format'] = $datetime_config['hour_format'];
      $date_only = [];
      $datetime_only = [];
      if(!empty($datetime_config['fields_datetime_only'])) {
        $datetimeArray = explode(',', $datetime_config['fields_datetime_only']);
        $trimmed_datetimeArray = array_map('trim',$datetimeArray);
        $datetime_only = array_filter($trimmed_datetimeArray);

      }
      if(!empty($datetime_config['fields_date_only'])) {
        $dateArray = explode(',', $datetime_config['fields_date_only']);
        $trimmed_dateArray = array_map('trim',$dateArray);
        $date_only = array_filter($trimmed_dateArray);

      }
    }

    // Get disabled days.
    $disabled_days = [];

    // Get active days.
    foreach ($element['#disable_days'] as $value) {
      if (!empty($value)) {
        // Exception for Sunday - should be 0 (on widget options need to be 7).
        $disabled_days[] = (int) $value < 8 ? (int) $value : 1;
      }
    }

    // Get excluded dates.
    $exclude_date = [];

    if (!empty($element['#exclude_date'])) {
      $exclude_date = explode("\n", $element['#exclude_date']);
    }

    // Default settings.
    $settings = [
      'data-hour-format' => $element['#hour_format'],
      'data-allow-times' => intval($element['#allow_times']),
      'data-first-day' => $first_day,
      'data-disable-days' => Json::encode($disabled_days),
      'data-week-start' => $element['#week_start'],
      'data-exclude-date' => $exclude_date,
    ];

    // Push field type to JS for changing between date only and time fields.
    // Difference between date and date range fields.
    if (isset($element['#date_type'])) {
      $settings['data-materialize-date-time'] = $element['#date_type'];
    }

    else {
      // Combine date range formats.
      $range_date_type = $element['#date_date_element'] . $element['#date_time_element'];
      $settings['data-materialize-date-time'] = $range_date_type;
    }

    // Append our attributes to element.
    $element['#attributes'] += $settings;
    $element['#attributes']['class'] = ['form-control'];

    // Prefix and Suffix.
    $element['#prefix'] = "<div class='container'>
    <div class='row'>
        <div class='col-sm-6'>";
    $element['#suffix'] = "</div></div></div>";

    // Attach library.
    $complete_form['#attached']['library'][] = 'materialize_datetime_picker/materialize_datetime_picker';
    if(count($datetime_only) > 0 || count($date_only) > 0) {
      $complete_form['#attached']['drupalSettings']['materialize_data'] = [
        'datetime_data' => $datetime_only,
        'date_data' => $date_only,
      ];
    }

    return $element;
  }

  /**
   * Return default settings. Pass in values to override defaults.
   *
   * @param array $values
   *   Some Desc.
   *
   * @return array
   *   Some Desc.
   */
  public static function settings(array $values = []) {
    $settings = [
      'lang' => 'en',
    ];

    return array_merge($settings, $values);
  }

}
