(function ($) {

	/**
   * Set active class on Views AJAX filter
   * on selected category
   */
  Drupal.behaviors.exposedfilter_buttons = {
    attach: function(context, settings) {
      $('.filter-tab a').once('exposedfilter_buttons').on('click', function(e) {
        e.preventDefault();

        // Get ID of clicked item
        var id = $(e.target).attr('id');

        // Set the new value in the SELECT element
        var filter = $('.views-exposed-form select[name="field_blog_categories_value"]');
        filter.val(id);

        // Unset and then set the active class
        $('.filter-tab a').removeClass('active');
        $(e.target).addClass('active');

        // Do it! Trigger the select box
        //filter.trigger('change');
        $('.views-exposed-form select[name="field_blog_categories_value"]').trigger('change');
        $('.views-exposed-form input.form-submit').trigger('click');

      });
    }
  };


	jQuery(document).ajaxComplete(function(event, xhr, settings) {
         // fancybox
        if(settings.extraData) {
          var view_data = settings.extraData;
          console.log(view_data.view_name);
          switch(view_data.view_name){

            case "blogs":
              var filter_id = $('.views-exposed-form select[name="field_blog_categories_value"]').find(":selected").val();
              $('.filter-tab a').removeClass('active');
              $('.filter-tab').find('#' + filter_id).addClass('active');

              break;

            default:
              break;
          };
        }

	});


})(jQuery);