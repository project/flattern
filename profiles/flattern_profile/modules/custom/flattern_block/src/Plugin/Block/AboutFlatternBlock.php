<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a 'About us' Block.
 *
 * @Block(
 *   id = "flattern_about_us",
 *   admin_label = @Translation("About flattern block"),
 *   category = @Translation("About flattern block"),
 * )
 */
class AboutFlatternBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler interface service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs for About flattern block configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    global $base_url;
    $this->moduleHandler = $module_handler;
    $this->module_path = $base_url . '/' . $this->moduleHandler->getModule('flattern_block')->getPath();
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['about_us_block_settings'])) {
      $text = $config['about_us_block_settings']['value'];
    }
    else {
      $text = $this->t('About flattern');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<p>
                      A108 Adam Street <br>
                      New York, NY 535022<br>
                      United States <br><br>
                      <strong>Phone:</strong> +1 5589 55488 55<br>
                      <strong>Email:</strong> info@example.com<br>
                    </p>';
    $form['about_us_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('This block show about us'),
      '#size' => 30,
      '#default_value' => !empty($config['about_us_block_settings']['value']) ? $config['about_us_block_settings']['value'] : $default_val,
      '#format' => 'restricted_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['about_us_block_settings'] = $form_state->getValue('about_us_block_settings');
  }

}
