<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a 'Our clients' Block.
 *
 * @Block(
 *   id = "flattern_our_clients",
 *   admin_label = @Translation("Our clients block"),
 *   category = @Translation("Our clients block"),
 * )
 */
class OurClientsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler interface service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs for our client configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    global $base_url;
    $this->moduleHandler = $module_handler;
    $this->module_path = $base_url . '/' . $this->moduleHandler->getModule('flattern_block')->getPath();
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['our_clients_block_settings'])) {
      $text = $config['our_clients_block_settings']['value'];
    }
    else {
      $text = $this->t('Our services');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<section id="clients" class="clients">
                      <div class="container">
                        <div class="section-title aos-init aos-animate" data-aos="fade-up">
                          <h2>Our <strong>Clients</strong></h2>
                          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                        </div>
                        <div class="row no-gutters clients-wrap clearfix aos-init aos-animate" data-aos="fade-up">
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-1.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-2.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-3.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-4.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-5.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-6.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-7.png" class="img-fluid" alt="">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="client-logo">
                              <img src="' . $this->module_path . '/images/clients/client-8.png" class="img-fluid" alt="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>';
    $form['our_clients_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('This block show our services'),
      '#size' => 30,
      '#default_value' => !empty($config['our_clients_block_settings']['value']) ? $config['our_clients_block_settings']['value'] : $default_val,
      '#format' => 'restricted_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['our_clients_block_settings'] = $form_state->getValue('our_clients_block_settings');
  }

}
