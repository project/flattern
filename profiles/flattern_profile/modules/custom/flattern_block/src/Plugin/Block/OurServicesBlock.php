<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'Our service' Block.
 *
 * @Block(
 *   id = "flattern_our_service",
 *   admin_label = @Translation("Our service block"),
 *   category = @Translation("Our service block"),
 * )
 */
class OurServicesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Form builder will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs for our Our service configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The formBuilderInterface interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['our_service_block_settings'])) {
      $text = $config['our_service_block_settings']['value'];
    }
    else {
      $text = $this->t('Our services');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<section id="services" class="services">
                      <div class="container">

                        <div class="section-title aos-init aos-animate" data-aos="fade-up">
                          <h2>Check out <strong>our services</strong></h2>
                        </div>

                        <div class="row">
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up">
                              <div class="icon"><i class="icofont-computer"></i></div>
                              <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                              <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
                              <h4 class="title"><a href="">Dolor Sitema</a></h4>
                              <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                              <div class="icon"><i class="icofont-earth"></i></div>
                              <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                              <div class="icon"><i class="icofont-image"></i></div>
                              <h4 class="title"><a href="">Magni Dolores</a></h4>
                              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                              <div class="icon"><i class="icofont-settings"></i></div>
                              <h4 class="title"><a href="">Nemo Enim</a></h4>
                              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-6">
                            <div class="icon-box aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
                              <div class="icon"><i class="icofont-tasks-alt"></i></div>
                              <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
                              <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
                            </div>
                          </div>
                        </div>

                      </div>
                    </section>';
    $form['our_service_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('This block show our services'),
      '#size' => 30,
      '#default_value' => !empty($config['our_service_block_settings']['value']) ? $config['our_service_block_settings']['value'] : $default_val,
      '#format' => 'restricted_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['our_service_block_settings'] = $form_state->getValue('our_service_block_settings');
  }

}
