<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a 'Useful Links' Block.
 *
 * @Block(
 *   id = "flattern_useful_links",
 *   admin_label = @Translation("Useful links block"),
 *   category = @Translation("Useful links block"),
 * )
 */
class UsefullLinkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler interface service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs for our client configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    global $base_url;
    $this->moduleHandler = $module_handler;
    $this->module_path = $base_url . '/' . $this->moduleHandler->getModule('flattern_block')->getPath();
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['usefull_link_block_settings'])) {
      $text = $config['usefull_link_block_settings']['value'];
    }
    else {
      $text = $this->t('Useful links');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<ul>
                      <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                      <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                      <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                      <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                      <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>';
    $form['usefull_link_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('This block show usefull links'),
      '#size' => 30,
      '#default_value' => !empty($config['usefull_link_block_settings']['value']) ? $config['usefull_link_block_settings']['value'] : $default_val,
      '#format' => 'restricted_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['usefull_link_block_settings'] = $form_state->getValue('usefull_link_block_settings');
  }

}
