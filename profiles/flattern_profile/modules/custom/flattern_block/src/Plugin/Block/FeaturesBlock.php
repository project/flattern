<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a 'Some Features we do provide' Block.
 *
 * @Block(
 *   id = "flattern_features",
 *   admin_label = @Translation("Features"),
 *   category = @Translation("Features"),
 * )
 */
class FeaturesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler interface service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs for Features configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    global $base_url;
    $this->moduleHandler = $module_handler;
    $this->module_path = $base_url . '/' . $this->moduleHandler->getModule('flattern_block')->getPath();
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['features_block_settings'])) {
      $text = $config['features_block_settings']['value'];
    }
    else {
      $text = $this->t('Some Features we do provide');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<section id="features" class="features">
                      <div class="container">

                        <div class="section-title" data-aos="fade-up">
                          <h2>Some <strong>Features</strong> we do provide</h2>
                          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                        </div>

                        <div class="row">
                          <div class="col-lg-4 mb-5 mb-lg-0" data-aos="fade-right">
                            <ul class="nav nav-tabs flex-column">
                              <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                  <h4>Modi sit est</h4>
                                  <p>Quis excepturi porro totam sint earum quo nulla perspiciatis eius.</p>
                                </a>
                              </li>
                              <li class="nav-item mt-2">
                                <a class="nav-link" data-toggle="tab" href="#tab-2">
                                  <h4>Unde praesentium sed</h4>
                                  <p>Voluptas vel esse repudiandae quo excepturi.</p>
                                </a>
                              </li>
                              <li class="nav-item mt-2">
                                <a class="nav-link" data-toggle="tab" href="#tab-3">
                                  <h4>Pariatur explicabo vel</h4>
                                  <p>Velit veniam ipsa sit nihil blanditiis mollitia natus.</p>
                                </a>
                              </li>
                              <li class="nav-item mt-2">
                                <a class="nav-link" data-toggle="tab" href="#tab-4">
                                  <h4>Nostrum qui quasi</h4>
                                  <p>Ratione hic sapiente nostrum doloremque illum nulla praesentium id</p>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-7 ml-auto" data-aos="fade-left" data-aos-delay="100">
                            <div class="tab-content">
                              <div class="tab-pane active show" id="tab-1">
                                <figure>
                                  <img src="' . $this->module_path . '/images/features-1.png" alt="" class="img-fluid">
                                </figure>
                              </div>
                              <div class="tab-pane" id="tab-2">
                                <figure>
                                  <img src="' . $this->module_path . '/images/features-2.png" alt="" class="img-fluid">
                                </figure>
                              </div>
                              <div class="tab-pane" id="tab-3">
                                <figure>
                                  <img src="' . $this->module_path . '/images/features-3.png" alt="" class="img-fluid">
                                </figure>
                              </div>
                              <div class="tab-pane" id="tab-4">
                                <figure>
                                  <img src="' . $this->module_path . '/images/features-4.png" alt="" class="img-fluid">
                                </figure>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </section>';
    $form['features_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('Some Features we do provide'),
      '#size' => 30,
      '#default_value' => !empty($config['features_block_settings']['value']) ? $config['features_block_settings']['value'] : $default_val,
      '#format' => 'restricted_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['features_block_settings'] = $form_state->getValue('features_block_settings');
  }

}
