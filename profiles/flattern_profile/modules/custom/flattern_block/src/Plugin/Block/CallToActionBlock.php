<?php

namespace Drupal\flattern_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a 'Call to action' Block.
 *
 * @Block(
 *   id = "flattern_calltoaction",
 *   admin_label = @Translation("Call to action"),
 *   category = @Translation("Call to action"),
 * )
 */
class CallToActionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler interface service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs for call to action configuration.
   *
   * @param array $configuration
   *   Block configuration.
   * @param string $plugin_id
   *   Plugin id configuration.
   * @param mixed $plugin_definition
   *   Plugin definition configuration.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    global $base_url;
    $this->moduleHandler = $module_handler;
    $this->module_path = $base_url . '/' . $this->moduleHandler->getModule('flattern_block')->getPath();
  }

  /**
   * Define service name.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface services.
   * @param array $configuration
   *   Configuration services.
   * @param string $plugin_id
   *   Plugin id services.
   * @param mixed $plugin_definition
   *   Plugin definition services.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['calltoaction_block_settings'])) {
      $text = $config['calltoaction_block_settings']['value'];
    }
    else {
      $text = $this->t('Call to action');
    }

    return [
      '#markup' => $text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $default_val = '<section id="cta-pricing" class="cta-pricing">
                      <div class="container">
                        <div class="text-center">
                          <h3>Call To Action</h3>
                          <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                          <a class="cta-btn" href="#">Call To Action</a>
                        </div>
                      </div>
                    </section>';
    $form['calltoaction_block_settings'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Please enter block description'),
      '#description' => $this->t('This block show Call To Action'),
      '#size' => 30,
      '#default_value' => !empty($config['calltoaction_block_settings']['value']) ? $config['calltoaction_block_settings']['value'] : $default_val,
      '#format' => 'full_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['calltoaction_block_settings'] = $form_state->getValue('calltoaction_block_settings');
  }

}
