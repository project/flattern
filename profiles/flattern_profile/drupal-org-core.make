core: 8.x
api: 2
projects:
  drupal:
    version: 8.9.7
  flattern_profile:
    type: profile
    custom_download: true
  admin_toolbar:
    version: '2.4'
  config_update:
    version: '1.7'
  ctools:
    version: '3.4'
  materialize_datetime_picker:
    version: '1.4'
  pathauto:
    version: '1.8'
  simplenews:
    version: 2.0-beta2
  token:
    version: '1.7'
  twig_tweak:
    version: '2.8'
  views_block_filter_block:
    version: '1.0'
  flattern_block:
    type: module
    custom_download: true
  flattern_blogs:
    type: module
    custom_download: true
  flattern_custom:
    type: module
    custom_download: true
  flattern_inner_page:
    type: module
    custom_download: true
  flattern_portfolio:
    type: module
    custom_download: true
  flattern_slider:
    type: module
    custom_download: true
  flattern_team:
    type: module
    custom_download: true
  flattern_testimonials:
    type: module
    custom_download: true
  flattern:
    type: theme
    custom_download: true
